##### Beacon Network Aggregator JAX-RS implementation.

The GET: /info endpoint returns the **service_info.json** file as the service description.  
The Aggregator uses known Beacon Network Registries predefined in the
**config.json** file:
```json
["https://my_registry/beacon-network/registry/v1.1.0"]
```

###### Open API 3.0
To enable Open API 3.0 descriptor generation, uncomment the **swagger-jaxrs2**
library in the **pom.xml** maven descriptor.  
Basic Open API 3.0 descriptor attributes are taken from the
**openapi-configuration.yaml** file.  
The generated Open API 3.0 desriptor will be delpoyed at ... /aggregator/v1.1.0/openapi.json
