/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.aggregator.rest;

import es.bsc.inb.ga4gh.beacon.model.v110.BeaconAlleleResponse;
import es.bsc.inb.ga4gh.beacon.network.aggregator.proxy.BeaconAggreagator;
import es.bsc.inb.ga4gh.beacon.network.model.v100.ServiceInfo;
import es.bsc.inb.ga4gh.beacon.network.model.v100.ServiceOrganization;
import es.bsc.inb.ga4gh.beacon.network.model.v100.ServiceTypes;
import es.bsc.inb.ga4gh.service_info.model.v100.Organization;
import es.bsc.inb.ga4gh.service_info.model.v100.Service;
import es.bsc.inb.ga4gh.service_info.model.v100.ServiceType;
import java.net.URI;
import java.security.Principal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 * @author Dmitry Repchevsky
 */

@Path("/")
@ApplicationScoped
public class BeaconAggregatorServices {

    @Inject
    private ServiceInfo service_info;
        
    @Inject
    private BeaconAggreagator proxy;

    @Resource
    private ManagedExecutorService executor;

    private Service service;
    
    @PostConstruct
    public void init() {        
        service = new Service();
            
        final String apiVersion = service_info.getApiVersion();
        final String serviceType = service_info.getServiceType();
        
        final ServiceType service_type = new ServiceType();
        service_type.setGroup("org.ga4gh");
        service_type.setVersion(apiVersion);
        
        if (ServiceTypes.GA4GHRegistry.name().equals(serviceType)) {
            service_type.setArtifact("service-registry");
        } else if (ServiceTypes.GA4GHBeaconAggregator.name().equals(serviceType)) {
            service_type.setArtifact("beacon-aggregator");
        } else if (ServiceTypes.GA4GHBeacon.name().equals(serviceType)) {
            service_type.setArtifact("beacon");
        }

        service.setType(service_type);

        service.setId(service_info.getId());
        service.setName(service_info.getName());
        service.setVersion(service_info.getVersion());
        service.setDescription(service_info.getDescription());

        final ServiceOrganization serviceOrganization = service_info.getOrganization();
        if (serviceOrganization != null) {
            final Organization organization = new Organization();

            organization.setId(serviceOrganization.getId());

            final String welcomeUrl = serviceOrganization.getWelcomeUrl();
            if (welcomeUrl != null) {
                try {
                    organization.setUrl(URI.create(welcomeUrl));
                } catch(IllegalArgumentException ex) {
                    Logger.getLogger(BeaconAggregatorServices.class.getName()).log(
                            Level.INFO, "invalid welcome url", ex.getMessage());
                }
            }

            service.setOrganization(organization);
        }

        final String createDateTime = service_info.getCreateDateTime();
        if (createDateTime != null) {
            try {
                service.setCreatedAt(ZonedDateTime.parse(createDateTime));
            } catch(DateTimeParseException ex) {
                    Logger.getLogger(BeaconAggregatorServices.class.getName()).log(
                            Level.INFO, "error parsing datetime", ex.getMessage());
            }
        }

        final String updateDateTime = service_info.getUpdateDateTime();
        if (updateDateTime != null) {
            try {
                service.setUpdatedAt(ZonedDateTime.parse(updateDateTime));
            } catch(DateTimeParseException ex) {
                    Logger.getLogger(BeaconAggregatorServices.class.getName()).log(
                            Level.INFO, "error parsing datetime", ex.getMessage());
            }
        }
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceInfo get(@Context UriInfo uri_info) {
        return getInfo(uri_info);
    }
    
    @GET
    @Path("/info")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceInfo getInfo(@Context UriInfo uri_info) {
        final String service_uri = UriBuilder.fromUri(uri_info.getBaseUri())
                .path(BeaconAggregatorServices.class).build().toString();
        service_info.setServiceUrl(service_uri);
        return service_info;
    }

    @GET
    @Path("/service-info")
    @Produces(MediaType.APPLICATION_JSON)
    public Service getServiceInfo() {
        return service;
    }

    @GET
    @Path("/service_types")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceTypes[] getServiceTypes() {
        return ServiceTypes.values();
    }

    @GET
    @Path("/services")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ServiceInfo> getServices(@Context SecurityContext sc,
            @QueryParam("serviceType") final String serviceType,
            @QueryParam("apiVersion") final String apiVersion) {
        
        List<ServiceInfo> services = proxy.getServices();

//        // if no login - filter "closed" services
//        if (sc.getUserPrincipal() == null) {
//            services = new ArrayList<>(services);
//            final Iterator<ServiceInfo> iter = services.iterator();
//            while (iter.hasNext()) {
//                if (!Boolean.TRUE.equals(iter.next().getOpen())) {
//                    iter.remove();
//                }
//            }
//        }
        
        if (serviceType == null) {
            if (apiVersion == null) {
                return services;
            }
            return services.stream().filter(c -> apiVersion.equals(c.getApiVersion()))
                    .collect(Collectors.toList());
        } else if (apiVersion == null) {
            return services.stream().filter(c -> serviceType.equals(c.getServiceType()))
                    .collect(Collectors.toList());
        } else {
            return services.stream()
                    .filter(c -> apiVersion.equals(c.getApiVersion()) && 
                            serviceType.equals(c.getServiceType()))
                    .collect(Collectors.toList());            
        }
    }

    @DELETE
    @Path("/services/")
    public void deleteServices() {
        proxy.reload();
    }

    @DELETE
    @Path("/services/{id}")
    public void deleteService(
            @PathParam("id") final String id,
            @Suspended final AsyncResponse asyncResponse) {
        
        final List<ServiceInfo> services = proxy.getServices();
        for (int i = 0; i < services.size(); i++) {
            if (id.equals(services.get(i).getId())) {
                services.remove(i);
                break;
            }
        }
    }

    @GET
    @Path("/query")
    @Produces(MediaType.APPLICATION_JSON)
    public void query(@Context UriInfo uriInfo,
                      @Context SecurityContext sc,
                      @QueryParam("referenceName") final String referenceName,
                      @QueryParam("start") final Long start,
                      @QueryParam("startMin") final Long startMin,
                      @QueryParam("startMax") final Long startMax,
                      @QueryParam("end") final Long end,
                      @QueryParam("endMin") final Long endMin,
                      @QueryParam("endMax") final Long endMax,
                      @QueryParam("referenceBases") final String referenceBases,
                      @QueryParam("alternateBases") final String alternateBases,
                      @QueryParam("variantType") final String variantType,
                      @QueryParam("assemblyId") final String assemblyId,
                      @QueryParam("datasetIds") final String datasetIds,
                      @QueryParam("includeDatasetResponses") final String includeDatasetResponses,
                      @Suspended final AsyncResponse asyncResponse) {

        final String query = uriInfo.getRequestUri().getQuery();
        final Principal principal = sc.getUserPrincipal();

        executor.submit(() -> {
            final BeaconAlleleResponse response = proxy.query(query, principal == null);
            
            response.setBeaconId(service_info.getId());
            response.setApiVersion(service_info.getApiVersion());
            
            asyncResponse.resume(Response.ok(response).build());
        });
    }
}
