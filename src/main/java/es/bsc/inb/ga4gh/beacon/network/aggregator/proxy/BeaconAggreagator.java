/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.aggregator.proxy;

import es.bsc.inb.ga4gh.beacon.model.v110.BeaconAlleleResponse;
import es.bsc.inb.ga4gh.beacon.model.v110.BeaconDatasetAlleleResponse;
import es.bsc.inb.ga4gh.beacon.model.v110.BeaconError;
import es.bsc.inb.ga4gh.beacon.network.aggregator.rest.BeaconAggregatorServices;
import es.bsc.inb.ga4gh.beacon.network.model.v100.ServiceInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconAggreagator {
    
    private final static String BEACON_AGGREGATOR_CONFIG_FILE = "config.json";
    
    @Inject 
    private ServletContext ctx;
    
    @Resource
    private ManagedExecutorService executor;

    private List<ServiceInfo> services;
    private String[] registry_endpoints;
    
    @PostConstruct
    public void init() {
        services = new ArrayList<>();
        try (InputStream in = ctx.getResourceAsStream(BEACON_AGGREGATOR_CONFIG_FILE)) {
            if (in == null) {
                Logger.getLogger(BeaconAggreagator.class.getName()).log(
                        Level.SEVERE, "no configuration file found: {0}", BEACON_AGGREGATOR_CONFIG_FILE);
            } else {
                registry_endpoints = JsonbBuilder.create().fromJson(in, String[].class);
                if (registry_endpoints != null) {
                    reload();
                } else {
                    registry_endpoints = new String[0];
                    Logger.getLogger(BeaconAggreagator.class.getName()).log(
                            Level.SEVERE, "no registries found in the {0}", BEACON_AGGREGATOR_CONFIG_FILE);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(BeaconAggreagator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ServiceInfo> getServices() {
        return services;
    }
    
    public void reload() {
        services.clear();

        for (int i = 0; i < registry_endpoints.length; i++) {
            final String registry_endpoint = registry_endpoints[i];
            if (registry_endpoint != null) {
                final List<ServiceInfo> list = getBeaconEndpoints(registry_endpoint);
                if (list != null) {
                    services.addAll(list);
                } else {
                    Logger.getLogger(BeaconAggreagator.class.getName()).log(
                            Level.SEVERE, "no services found at the {0}", registry_endpoint);
                }
            }
        }
    }
    
    private List<ServiceInfo> getBeaconEndpoints(final String registry_endpoint) {
        Logger.getLogger(BeaconAggreagator.class.getName()).log(
                Level.INFO, "getting services from {0}...", registry_endpoint);
        
        final Client client = ClientBuilder.newClient();
        try {
            return client.target(UriBuilder.fromUri(registry_endpoint).path("services"))
                    .request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<ServiceInfo>>(){});
        } catch(Exception ex) {
            Logger.getLogger(BeaconAggreagator.class.getName()).log(
                    Level.SEVERE, "error getting beacon endpoints from {0}\n{1}", 
                    new String[] {registry_endpoint, ex.getMessage()});
        }
        return null;
    }
    
    public BeaconAlleleResponse query(final String query, final boolean isPublic) {
        final BeaconAlleleResponse response = new BeaconAlleleResponse();
        
        if (services.isEmpty()) {
            final BeaconError error = new BeaconError();
            error.setErrorCode(500);
            response.setError(error);
            return response;
        }
        
        final List<BeaconDatasetAlleleResponse> responses = new CopyOnWriteArrayList();
        
        final CountDownLatch latch = new CountDownLatch(services.size());
        for (ServiceInfo service_info : services) {
//          if (Boolean.TRUE.equals(service_info.getOpen()) || !isPublic) {
            final String endpoint = service_info.getServiceUrl();
            if (endpoint == null || endpoint.isEmpty()) {
                Logger.getLogger(BeaconAggreagator.class.getName()).log(
                        Level.SEVERE, "no serviceUrl in {0} found", service_info.getId());
                continue;
            }
            try {
                executor.submit(() -> {
                    try {
                        Logger.getLogger(BeaconAggregatorServices.class.getName()).log(
                                Level.INFO, "querying to {0}", endpoint);
                        final BeaconAlleleResponse res = proxy(query, endpoint);
                        if (res != null) {
                            final Boolean exists = res.getExists();
                            if (exists != null && exists) {
                                response.setExists(true);
                            }
                            final List<BeaconDatasetAlleleResponse> list = res.getDatasetAlleleResponses();
                            if (list != null) {
                                responses.addAll(list);
                            }
                        }
                    } catch (Throwable th) {
                        Logger.getLogger(BeaconAggreagator.class.getName())
                                .log(Level.SEVERE, "{0}/query fails with {1}", 
                                        new Object[] {endpoint, th.getMessage()});
                    }
                    latch.countDown();
                });
            } catch(Throwable th) {
                Logger.getLogger(BeaconAggreagator.class.getName()). log(Level.SEVERE, "query", th);
                latch.countDown();
            }
        }
        
        try {
            latch.await(5, TimeUnit.MINUTES);
        } catch (InterruptedException ex) {
        }

        response.setDatasetAlleleResponses(responses);
        
        return response;
    }
    
    private BeaconAlleleResponse proxy(
            final String query,
            final String endpoint) {
        
        final Client client = ClientBuilder.newClient();
        try {
            return client.target(UriBuilder.fromUri(endpoint).path("query").replaceQuery(query))
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(BeaconAlleleResponse.class);
        } catch(Exception ex) {
            Logger.getLogger(BeaconAggreagator.class.getName()).log(
                    Level.SEVERE, "error getting beacon endpoints from {0}", endpoint);
        }
        return null;
    }
}
